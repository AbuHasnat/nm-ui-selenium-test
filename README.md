# NM-UI-SELENIUM-TEST



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!



```
cd existing_repo
git remote add origin https://gitlab.com/AbuHasnat/nm-ui-selenium-test.git
git branch -M main
git push -uf origin main
```

## Pre prerequisite


- Java (JDK+JRE, version 1.8)
- Java programming
- Gitlab account
- Git configured with your system
- Installation
- IntelliJ community edition IDE
a. Comes with maven as build tool
b. Plugins that needs to install in IntelliJ
(File&gt;Settings&gt;Plugins)


## Test and Deploy

- Clone it to local machine
- Go to the cloned folder
- Open the folder in Eclipse or IntelliJ
- Give some time to load the dependencies
- Go to src/test/java from the package explorer
- Open the package “userJourney” - you will see three classes inside.


***

